import { assert } from 'chai';
import Checkout from '../src/lib/checkout';
import loadProductionInfo from '../src/lib/loadProductInfo'
require('mocha-sinon');

describe("Checkout", () => {
  let checkout;
  describe("Bulk should get lower price", () => {
    before(() => {
      const productInfo = loadProductionInfo()
      checkout = new Checkout(productInfo)
      checkout.scan(1);
      checkout.scan(1);
      checkout.scan(1);
      checkout.scan(1);
    });

    it('should return if called calculateTotal', function () {
      assert.equal(2199.96, checkout.calculateTotal());
    });
  });

  describe("When bundle", () => {
    before(() => {
      const productInfo = loadProductionInfo()
      checkout = new Checkout(productInfo)
      checkout.scan(2);
    });

    it('should get bundled deal if called calculateTotal', function () {
      assert.equal(1399.99, checkout.calculateTotal());
    });

  });

  describe("When purchased three cari", () => {
    before(() => {
      const productInfo = loadProductionInfo()
      checkout = new Checkout(productInfo)
      checkout.scan(3);
      checkout.scan(3);
      checkout.scan(3);
    });

    it('should get n for m if called calculateTotal', function () {
      assert.equal(328.5, checkout.calculateTotal());
    });
  });
});
