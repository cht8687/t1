# Install dependency

```bash
npm install 
```

# Run the program

```bash

npm run 

```

# Run the test

```
npm run test

```
