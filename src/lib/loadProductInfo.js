import bulk from '../policy/bulk';
import bundle from '../policy/bundle';
import nform from '../policy/n_for_m.json';
import products from '../policy/products.json';

export default () => {
  return {
    bulk: bulk.policies,
    bundle: bundle.policies,
    nform: nform.policies,
    products: products.products
  }
}
