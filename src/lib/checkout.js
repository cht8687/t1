import chalk from "chalk"
import Shoppingcart from "./shoppingCart"
import { shoppingItem } from "./shoppingItem"
const log = console.log
export default class Checkout {
  constructor(productInfo) {
    this._productInfo = productInfo
    this._bulk = productInfo.bulk
    this._bundle = productInfo.bundle
    this._nform = productInfo.nform
    this._products = productInfo.products
    this._shoppingcart = new Shoppingcart()
  }

  // Function to scan item  
  scan(idnum) {
    const id = parseInt(idnum)
    if (id > 0 && id <= this._products.length) {
      const itemInfo = this.findItemById(id)
      this._shoppingcart.add(shoppingItem(id, itemInfo.price, false))
      // check if is eligible to have free bundles after each scan
      this.applyBundlePolicy(id)
      this.printShopingCart()
    } else {
      log(chalk.red('Item number does not exit! Please chose number'))
    }
  }

  findItemById(id) {
    return this._products.find(item => item.id === id)
  }

  applyBulkPolicy() {
    this._bulk.map(item => {
      // find each bulk item 
      const bulkItems = this._shoppingcart.get().filter(cartItem => cartItem.id === item.product_id)
      if (bulkItems.length > 0) {
        // meet the policy, we are going to update each item's price
        if (bulkItems.length >= item.min_purchase) {
          this._shoppingcart.updatePriceById(item.product_id, item.discounted_price)
        }
      }
    })
  }

  applyBundlePolicy(id) {
    const bundleItem = this._bundle.find(bundleItem => id === bundleItem.eligible_product_id)
    if (bundleItem) {
      this._shoppingcart.add(shoppingItem(bundleItem.free_bundle_product_id, 0, true))
    }
  }

  applyNforMPolicy() {
    this._nform.map(item => {
      // find each bulk item 
      const eligibleItems = this._shoppingcart.get().filter(cartItem => cartItem.id === item.product_id)
      if (eligibleItems.length > 0) {
        // meet the policy, we are going to mark eligible item to price 0
        if (eligibleItems.length >= item.eligible_number) {
          this._shoppingcart.waiveItemsPrice(item.product_id, item.free_deduct)
        }
      }
    })
  }

  calculateTotal() {
    let total = 0;
    this._shoppingcart.get().map(item => {
      total += item.price
    })
    return total
  }

  printShopingCart() {
    let ids = this._shoppingcart.get().map(item => {
      return item.id;
    })
    const names = ids.map(id => {
      return `${this.findItemById(id).name} `
    })
    log(chalk.green(`current cart: [ ${[...names]} ]`))
  }

  checkout() {
    // apply bulk policies first, which means, we need deduct the price
    this.applyBulkPolicy()
    // apply buy n only pay for m policy
    this.applyNforMPolicy()
    // log('Total expected: ' + JSON.stringify(this._shoppingcart.get()))
    log(chalk.cyan('Total expected: $' + chalk.white.bgGreen(`${this.calculateTotal().toFixed(2)}`)))
  }
} 
