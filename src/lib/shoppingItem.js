export const shoppingItem = (id, price, isFreeBundle) => {
  return {
    'id': id,
    'price': price,
    'isFreeBundle': isFreeBundle
  }
}
