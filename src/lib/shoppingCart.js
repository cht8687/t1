export default class Shoppingcart {
  constructor() {
    this._items = []
  }

  get() {
    return this._items
  }

  set(items) {
    this._items = []
    this._items = items
  }

  updatePriceById(id, price) {
    this._items = this._items.map(item => {
      if (item.id === id) {
        item.price = price
      }
      return item
    })
  }

  waiveItemsPrice(id, numToWaive) {
    this._items = this._items.map(item => {
      // if id match and item is not free and quota not used
      if (item.id === id && !item.isFreeBundle && numToWaive > 0) {
        console.log(item.id)
        console.log(item.isFreeBundle)
        console.log(numToWaive)
        item.price = 0
        numToWaive--;
      }
      return item
    })
  }

  add(item) {
    this._items.push(item)
  }
}
