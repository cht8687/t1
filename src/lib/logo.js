import chalk from 'chalk'
const log = console.log;

export default () => {
  log(chalk.magenta(
   `                                   
    iii ..  .  . .  . .  . .  . .  . .  .  
    iiiii:          .         .         .  .
    iiiii  .  aaaaaaaaaaaa   .ggggggggggg
    iiii;   aaaaaaaaaaaaaaa  .gggggggggggggg
    iiiii aaaaaaaaaaaaaaaaa:gggggggggggggggg
    iiii..aaaaaaa   aaaaaaa;ggggggg   gggggg
    iiiiaaaaaaa      aaaaaaaggggg      ggggg
    iiiiaaaa ...    aaaaaa;ggggg      gggggg
    iiiii;aaaaaa.   aaaaaaaagggggg   gggggg
    iiii .aaaaaaa aaaaaaaaa:gggggggggggggggg.
    iiiii:.; aaaaaaaaaaaaaaa:gggggggggggggg  
     iiii  .:aaaaaaaaa aaaa:  ggggggg  ggg 
     ..   . .  . .:; ::..:.  gggggggggggggg  
      . .    .   .     .   . ggd     gggg.
           .  .    .  .  .  .ggg    gggg  
      .  .   .  .       .    gggggggggg
           .   .  . .     . .gggggggggg
      . .    .        . . .   gggggggg
    
    '`
  ))
  log(chalk.blue.bgWhite('===Welcome to IAG checkout system===='))
}
