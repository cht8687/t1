import chalk from 'chalk' 
const log = console.log 

export default class Menu {
  constructor(products) {
    this._products = products
  }

  // print items  
  print() {
    log(chalk.cyan('------------------------------------------------'))
    this._products.forEach(item => {
      log(chalk.yellow(`${item.id}: ${item.name} - $${item.price}`))
    })
  }
}
