import chalk from 'chalk'
import logo from './lib/logo'
import loadProductionInfo from './lib/loadProductInfo'
import Checkout from './lib/checkout'
import Menu from './lib/menu'
import readline from 'readline'
const log = console.log

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function main() {
  logo()
  log(chalk.yellow('Current time is ') + (new Date().toLocaleString()))
  // load product information
  const productInfo = loadProductionInfo()
  // create menu 
  const menu = new Menu(productInfo.products)
  // create checkout 
  const checkout = new Checkout(productInfo)
  menu.print()
  rl.setPrompt('Please item to purchase (q to checkout):')
  rl.prompt();
  rl.on('line', function (line) {
    if (line === "q") rl.close();
    checkout.scan(line)
    menu.print()
    rl.prompt();
  }).on('close', function () {
    checkout.checkout()
    process.exit(0)
  });
}

main()
